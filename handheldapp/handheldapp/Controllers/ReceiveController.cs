﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using handheldapp.Model ;

namespace handheldapp.Controllers
{
    public struct Reply
    {
        public int sCode;
        public string payload;
     }
    static public class ReceiveController
    {
        public static PurchaseOrder getminimpodById(string id)
        {
            return new PurchaseOrder(id, new Client("client_id","client_name"), DateTime.Now);    
        
        }
        public static List<PurchaseOrderItem> getpod_itemsByPod_Id(string id)
        {

            List<PurchaseOrderItem> itemsList = new List<PurchaseOrderItem>();
            for (int i = 0; i < 5; i++) {
               itemsList.Add( new PurchaseOrderItem("pod_item_id", new ItemData("item_id" + i, "item_id" + i, "5565"+i), (i + 1) * 10,""+i));
            }

            return itemsList; 
        }
        public static PurchaseOrder getfullpodById(string id)
        {
            ///To be re implemnted using dictionary
            ///
            PurchaseOrder pod=  getminimpodById(id);
            pod.Items = getpod_itemsByPod_Id(id);
            return pod; 
        }

        public static String generateAreceivingJobSerial() {

            return DateTime.Now.ToString();
        
        }

        public static String generateAstorageJobSerial()
        {

            return DateTime.Now.ToString();

        }

        public static Reply SubmitReceivigJob(string RCJ_Id, string POI_Id, int amount, string itemSerial,
                                              string UL_Serial, int ItemQulatiyStatus, DateTime productionDate) {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200; 

            
            return response; 

        }

        public static Reply SubmitStorageJob(string STJ_Id, string stlSerial,string UL_Serial)
        {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200;


            return response;

        }

    }
}
