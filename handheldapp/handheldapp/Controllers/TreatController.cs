﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using handheldapp.Model ;

namespace handheldapp.Controllers
{
    
    static public class TreatController
    {
        public static List<TreatIn> getTreatIns()
        {

            List<TreatIn> itemsList = new List<TreatIn>();
            for (int i = 0; i < 5; i++) {
                UnitLoad ul = new UnitLoad (i+"", "UL " + i, "" + i);
                ItemData itD= new ItemData("item_id" + i, "item_id" + i, "5565" + i);
                itemsList.Add(new TreatIn("Id "+i,ul,itD));
            }

            return itemsList; 
        }

        public static List<TreatOut> getTreatOuts()
        {
            List<TreatOut> itemsList = new List<TreatOut>();
            for (int i = 5; i < 10; i++) {
                UnitLoad ul = new UnitLoad(i+"", "UL " + i, "" + i);
                ItemData itD= new ItemData("item_id" + i, "item_id" + i, "5565" + i);
                itemsList.Add(new TreatOut("Id "+i,ul,itD));
            }
            return itemsList;
        }
      
        public static Reply SubmitReceivigJob(string RCJ_Id, string POI_Id, int amount, string itemSerial,
                                              string UL_Serial, int ItemQulatiyStatus, DateTime productionDate) {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200; 

            
            return response; 

        }

        public static Reply SubmitStorageJob(string STJ_Id, string stlSerial,string UL_Serial)
        {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200;


            return response;

        }

    }
}
