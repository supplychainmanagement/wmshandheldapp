﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using handheldapp.Model ;

namespace handheldapp.Controllers
{
    
    static public class IssueOrderController
    {
        public static IssueOrder getminimIodById(string id)
        {
            return new IssueOrder(id, new Client("client_id", "client_name"), DateTime.Now,new Customer("customer_id","customer_name"));    
        
        }

        public static List<IssueOrderItem> getIod_itemsByIod_Id(IssueOrder id)
        {

            List<IssueOrderItem> itemsList = new List<IssueOrderItem>();
            for (int i = 0; i < 5; i++) {
                itemsList.Add(new IssueOrderItem("iod_item_id", new ItemData("item_id" + i, "item_id" + i, "5565" + i), (i + 1) * 10, "" + i));
                itemsList[i].iod = id; 
            }

            return itemsList; 
        }

        public static IssueOrder getfullIodById(string id)
        {
            ///To be re implemnted using dictionary
            ///
            IssueOrder iod=  getminimIodById(id);
            iod.Items = getIod_itemsByIod_Id(iod);
            return iod; 
        }

        

        public static String generateAshippingJob()
        {

            return DateTime.Now.ToString();

        }

        public static Reply SubmitReceivigJob(string RCJ_Id, string POI_Id, int amount, string itemSerial,
                                              string UL_Serial, int ItemQulatiyStatus, DateTime productionDate) {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200; 

            
            return response; 

        }

        public static Reply SubmitStorageJob(string STJ_Id, string stlSerial,string UL_Serial)
        {
            Reply response = new Reply();
            response.payload = "Verified";
            response.sCode = 200;


            return response;

        }

    }
}
