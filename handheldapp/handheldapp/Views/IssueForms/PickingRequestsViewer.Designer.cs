﻿namespace handheldapp.Views.IssueForms
{
    partial class PickingRequestsViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.inTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.inDataGrid1 = new System.Windows.Forms.DataGrid();
            this.outTabControl = new System.Windows.Forms.TabPage();
            this.outDataGrid1 = new System.Windows.Forms.DataGrid();
            this.backButton = new System.Windows.Forms.Button();
            this.inTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.outTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // inTabControl
            // 
            this.inTabControl.Controls.Add(this.tabPage1);
            this.inTabControl.Controls.Add(this.outTabControl);
            this.inTabControl.Location = new System.Drawing.Point(3, 3);
            this.inTabControl.Name = "inTabControl";
            this.inTabControl.SelectedIndex = 0;
            this.inTabControl.Size = new System.Drawing.Size(255, 303);
            this.inTabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.inDataGrid1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(247, 274);
            this.tabPage1.Text = "In Requests";
            // 
            // inDataGrid1
            // 
            this.inDataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.inDataGrid1.Location = new System.Drawing.Point(0, 3);
            this.inDataGrid1.Name = "inDataGrid1";
            this.inDataGrid1.Size = new System.Drawing.Size(244, 271);
            this.inDataGrid1.TabIndex = 0;
            this.inDataGrid1.DoubleClick += new System.EventHandler(this.outDataGrid1_DoubleClick);
            // 
            // outTabControl
            // 
            this.outTabControl.Controls.Add(this.outDataGrid1);
            this.outTabControl.Location = new System.Drawing.Point(4, 25);
            this.outTabControl.Name = "outTabControl";
            this.outTabControl.Size = new System.Drawing.Size(247, 274);
            this.outTabControl.Text = "Out Requests";
            // 
            // outDataGrid1
            // 
            this.outDataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.outDataGrid1.Location = new System.Drawing.Point(0, 3);
            this.outDataGrid1.Name = "outDataGrid1";
            this.outDataGrid1.Size = new System.Drawing.Size(247, 268);
            this.outDataGrid1.TabIndex = 0;
            this.outDataGrid1.DoubleClick += new System.EventHandler(this.outDataGrid1_DoubleClick);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(23, 316);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(212, 20);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "Back";
            this.backButton.Click += new System.EventHandler(this.backButton_Click_1);
            // 
            // PickingRequestsViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(258, 347);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.inTabControl);
            this.Menu = this.mainMenu1;
            this.Name = "PickingRequestsViewer";
            this.Text = "Treat Requests";
            this.Load += new System.EventHandler(this.PickingRequestsViewer_Load);
            this.inTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.outTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl inTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage outTabControl;
        private System.Windows.Forms.DataGrid inDataGrid1;
        private System.Windows.Forms.DataGrid outDataGrid1;
        private System.Windows.Forms.Button backButton;
    }
}