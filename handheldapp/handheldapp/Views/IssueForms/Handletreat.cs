﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Model ;
using handheldapp.Controllers;

namespace handheldapp.Views.IssueForms
{
    public partial class Handletreat : Form
    {

        Treat currentTreat;
        int step = 0; //0 is now scanning the item , 1 is scanning the unitLoad
        string ItemSerial;
        string UL_Serial;
        String PkJSerial;

        public Handletreat(Treat currentTreat_ )
        {
            InitializeComponent();
            GlobalData.previous_form = new PickingRequestsViewer();
            this.currentTreat = currentTreat_;
            this.PkJSerial = PickingController.generateUniguePickingJob();
            this.stjnoLabel.Text = PkJSerial;
            
        }

        private void backButton_Click_1(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void ulSerialGoButton_Click(object sender, EventArgs e)
        {

        }

        private void stlSerialTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ul_Serial_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void itemSerialLabel_ParentChanged(object sender, EventArgs e)
        {

        }

        private void ulSerialGoButton_Click_1(object sender, EventArgs e)
        {
            {
                if (step == 0)// scanning the item 
                {
                    if (!basicValidation()) return;
                    if (!logicValidation()) return;
                    //Visible the the item controls 
                    ulSerialLabel.Visible = true;
                    ul_Serial_textBox.Visible = true;
                    //DeVisible the Ul Controls
                    itemSerialLabel.Visible = false ;
                    itemSerialTextBox.Visible = false;
                    step++;
                }
                else if (step == 1)
                {

                    if (!basicValidation()) return;
                    if (!logicValidation()) return;


                    Reply returnValue = PickingController.SubmitPickingJob(currentTreat.id,ItemSerial,UL_Serial,PkJSerial);
                    String ResultViewer = "Reply is " + returnValue.sCode + " " + returnValue.payload + "\n" +
                                           " " + itemSerialTextBox + " " + UL_Serial + " " + currentTreat.id + "";
                    MessageBox.Show(ResultViewer);
                }

            }


        }

        private void itemSerialTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ItemSerial = this.itemSerialTextBox.Text; 
        }

        private void ul_Serial_textBox_TextChanged_1(object sender, EventArgs e)
        {
            this.UL_Serial = this.ul_Serial_textBox.Text;
        }

        private bool basicValidation()
        {

            if (step == 0)
            {
                //item serial Basic Validation.
                if (ItemSerial == null || ItemSerial == "")
                {
                    MessageBox.Show("Please scan the Item Serial", "[[Error]]");
                    return false;
                }
                if (!GlobalData.IsDigitsOnly(ItemSerial))
                {
                    MessageBox.Show("Item Serial is not in correct format", "[[Error]]");
                    return false;
                }
            }

            if (step == 1)
            {
                // itemserialValidation validation  
                if (UL_Serial == null || UL_Serial == "")
                {
                    MessageBox.Show("Please scan the unitload", "[[Error]]");
                    return false;
                }

                if (!GlobalData.IsDigitsOnly(UL_Serial))
                {
                    MessageBox.Show("Unitload Serial is not in correct format", "[[Error]]");
                    return false;
                }
            }
            return true;
        }

        private bool logicValidation()
        {
            // Check the same Treat Values. 

            if (step == 0)
            {
                if (currentTreat.thisItemdata.Serial != ItemSerial)
                {
                    MessageBox.Show("Please scan the correct Item", "[[Error]]");
                    return false;
                }
            }
            else {
                if (currentTreat.thisUnitLoad.Serial != UL_Serial)
                {
                    MessageBox.Show("Please scan the correct unitload", "[[Error]]");
                    return false;
                }
            }
 
            return true;
        }
    }
}