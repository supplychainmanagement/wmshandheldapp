﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Controllers;
using handheldapp.Views.IssueForms;
namespace handheldapp.Views
{
    public partial class IssueMenu : Form
    {
        public IssueMenu()
        {
            InitializeComponent();
        }

        private void ReceivingMenu_Load(object sender, EventArgs e)
        {
            GlobalData.previous_form = new homescreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IssueOrderViewer newForm = new IssueOrderViewer();
            this.Hide();
            newForm.Show();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PickingRequestsViewer newForm = new PickingRequestsViewer();
            this.Hide();
            newForm.Show();
        }
    }
}