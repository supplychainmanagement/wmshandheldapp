﻿namespace handheldapp.Views
{
    partial class IssueOrderViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.podPanel = new System.Windows.Forms.Panel();
            this.goButton = new System.Windows.Forms.Button();
            this.customerText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.datetext = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.clientText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.iodserialTextView = new System.Windows.Forms.TextBox();
            this.itemsPanel = new System.Windows.Forms.Panel();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.switchButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.podPanel.SuspendLayout();
            this.itemsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 33);
            this.label1.Text = "Scan the Issue order serial or enter it below";
            this.label1.ParentChanged += new System.EventHandler(this.label1_ParentChanged);
            // 
            // podPanel
            // 
            this.podPanel.Controls.Add(this.goButton);
            this.podPanel.Controls.Add(this.customerText);
            this.podPanel.Controls.Add(this.label4);
            this.podPanel.Controls.Add(this.datetext);
            this.podPanel.Controls.Add(this.label3);
            this.podPanel.Controls.Add(this.clientText);
            this.podPanel.Controls.Add(this.label2);
            this.podPanel.Controls.Add(this.iodserialTextView);
            this.podPanel.Controls.Add(this.label1);
            this.podPanel.Location = new System.Drawing.Point(15, 15);
            this.podPanel.Name = "podPanel";
            this.podPanel.Size = new System.Drawing.Size(215, 233);
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(142, 62);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(57, 23);
            this.goButton.TabIndex = 13;
            this.goButton.Text = "Go";
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // customerText
            // 
            this.customerText.Location = new System.Drawing.Point(84, 197);
            this.customerText.Name = "customerText";
            this.customerText.ReadOnly = true;
            this.customerText.Size = new System.Drawing.Size(115, 23);
            this.customerText.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(17, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.Text = "Customer";
            // 
            // datetext
            // 
            this.datetext.Location = new System.Drawing.Point(84, 151);
            this.datetext.Name = "datetext";
            this.datetext.ReadOnly = true;
            this.datetext.Size = new System.Drawing.Size(115, 23);
            this.datetext.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.Text = "Date";
            // 
            // clientText
            // 
            this.clientText.Location = new System.Drawing.Point(84, 111);
            this.clientText.Name = "clientText";
            this.clientText.ReadOnly = true;
            this.clientText.Size = new System.Drawing.Size(115, 23);
            this.clientText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.Text = "Client";
            // 
            // iodserialTextView
            // 
            this.iodserialTextView.Location = new System.Drawing.Point(17, 62);
            this.iodserialTextView.Name = "iodserialTextView";
            this.iodserialTextView.Size = new System.Drawing.Size(119, 23);
            this.iodserialTextView.TabIndex = 1;
            // 
            // itemsPanel
            // 
            this.itemsPanel.Controls.Add(this.dataGrid1);
            this.itemsPanel.Enabled = false;
            this.itemsPanel.Location = new System.Drawing.Point(12, 18);
            this.itemsPanel.Name = "itemsPanel";
            this.itemsPanel.Size = new System.Drawing.Size(215, 191);
            this.itemsPanel.Visible = false;
            this.itemsPanel.GotFocus += new System.EventHandler(this.itemsPanel_GotFocus);
            this.itemsPanel.EnabledChanged += new System.EventHandler(this.itemsPanel_EnabledChanged);
            // 
            // dataGrid1
            // 
            this.dataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid1.Location = new System.Drawing.Point(3, 7);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(212, 184);
            this.dataGrid1.TabIndex = 0;
            this.dataGrid1.DoubleClick += new System.EventHandler(this.dataGrid1_DoubleClick);
            // 
            // switchButton
            // 
            this.switchButton.Location = new System.Drawing.Point(79, 267);
            this.switchButton.Name = "switchButton";
            this.switchButton.Size = new System.Drawing.Size(72, 20);
            this.switchButton.TabIndex = 2;
            this.switchButton.Text = "View Items";
            this.switchButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(18, 303);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(212, 20);
            this.backButton.TabIndex = 5;
            this.backButton.Text = "Back";
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // IssueOrderViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(264, 250);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.switchButton);
            this.Controls.Add(this.itemsPanel);
            this.Controls.Add(this.podPanel);
            this.Menu = this.mainMenu1;
            this.Name = "IssueOrderViewer";
            this.Text = "Issue Orders";
            this.podPanel.ResumeLayout(false);
            this.itemsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel podPanel;
        private System.Windows.Forms.Button switchButton;
        private System.Windows.Forms.TextBox iodserialTextView;
        private System.Windows.Forms.TextBox customerText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox datetext;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox clientText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel itemsPanel;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.Button backButton;
    }
}