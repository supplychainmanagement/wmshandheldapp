﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Controllers;
using handheldapp.Model;
using handheldapp.Views.IssueForms; 

namespace handheldapp.Views
{
    public partial class IssueOrderViewer : Form
    {
        int panelIndex = 0; 
        Panel [] panels = new Panel[2];

        public IssueOrderViewer()
        {
            InitializeComponent();
            panels[0] = this.podPanel;
            panels[1] = this.itemsPanel;
            GlobalData.previous_form = new IssueMenu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button senderbut = sender as Button;

            if (senderbut.Text.Equals("View Items")) senderbut.Text = "View Order"; else senderbut.Text = "View Items"; 
            podPanel.Visible = ! podPanel.Visible;
            itemsPanel.Visible = !itemsPanel.Visible;

            podPanel.Enabled = !podPanel.Enabled;
            itemsPanel.Enabled = !itemsPanel.Enabled;
        }

        private void label1_ParentChanged(object sender, EventArgs e)
        {

        }

        private void goButton_Click(object sender, EventArgs e)
        {
            string serial = iodserialTextView.Text;
            IssueOrder iod = IssueOrderController.getminimIodById(serial);
            this.clientText.Text = iod.Client.Name;
            this.customerText.Text = iod.Customer.Name;
            this.datetext.Text = iod.IodDate.ToString(); 
        }

        private void itemsPanel_GotFocus(object sender, EventArgs e)
        {

        }

        private void itemsPanel_EnabledChanged(object sender, EventArgs e)
        {
            string serial = iodserialTextView.Text;
            var list = IssueOrderController.getIod_itemsByIod_Id(IssueOrderController.getminimIodById(serial));
            var bindingList = new BindingList<IssueOrderItem>(list);
            BindingSource source = new BindingSource(bindingList, null);
            
            dataGrid1.DataSource = bindingList;
            
            
           
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void dataGrid1_DoubleClick(object sender, EventArgs e)
        {
            BindingList<IssueOrderItem> mylist = (BindingList<IssueOrderItem>)dataGrid1.DataSource;
            IssueOrderItem currentObject = (IssueOrderItem)mylist[dataGrid1.CurrentCell.RowNumber];
            string jobSerial  =ReceiveController.generateAreceivingJobSerial(); 
            DialogResult dialogResult = MessageBox.Show ("Proceed with Receiving Job no "+jobSerial, "[[Auto Genrated]]", MessageBoxButtons.YesNo,MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialogResult == DialogResult.Yes)
            {
                //do something
                this.Hide();
                new ShippingForm(currentObject).Show(); 

            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }

        }

       
    }
}