﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Model ;
using handheldapp.Controllers;

namespace handheldapp.Views.IssueForms
{
    public partial class ShippingForm : Form
    {
        DateTime orderDate;
        string driverName;
        string shippingAgency; 

        public ShippingForm(IssueOrderItem item)
        {
            InitializeComponent();
            GlobalData.previous_form = new IssueOrderViewer();
            this.iodNoLabel.Text = item.iod.OrderNo;
            this.itemSriallabel.Text = item.ItemName; 
            this.qtyLabel.Text = ""+item.Amount;
            this.shipJobId.Text = "" + ShippingController.generateAshippingJob();
        }

        private void backButton_Click_1(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {

            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            
        }

        private void dNameTextBox_TextChanged(object sender, EventArgs e)
        {
            this.driverName = this.dNameTextBox.Text; 
        }

        private void shippingAgencyTextBox_TextChanged(object sender, EventArgs e)
        {
            this.shippingAgency = this.shippingAgencyTextBox.Text;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            this.orderDate = this.dateTimePicker1.Value;
        }
    }
}