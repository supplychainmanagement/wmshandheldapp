﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Model;
using handheldapp.Controllers;
using handheldapp.Views;

namespace handheldapp.Views.IssueForms
{
    public partial class PickingRequestsViewer : Form
    {
        public PickingRequestsViewer()
        {
            InitializeComponent();
            GlobalData.previous_form = new IssueMenu();
        }

        private void PickingRequestsViewer_Load(object sender, EventArgs e)
        {
            
            var list = TreatController.getTreatIns();
            var bindingList = new BindingList<TreatIn>(list);
            BindingSource source = new BindingSource(bindingList, null);

            inDataGrid1.DataSource = bindingList;
            var list_1 = TreatController.getTreatOuts();
            var bindingList_1 = new BindingList<TreatOut>(list_1);
            BindingSource source_1 = new BindingSource(bindingList_1, null);

            outDataGrid1.DataSource = bindingList_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {

        }

        private void backButton_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void outDataGrid1_DoubleClick(object sender, EventArgs e)
        {
            DataGrid datagrid = sender as DataGrid;
            Treat currentObject =new Treat();
            if (datagrid == this.inDataGrid1)
            {

                BindingList<TreatIn> mylist = (BindingList<TreatIn>)datagrid.DataSource;
               currentObject = (TreatIn)mylist[datagrid.CurrentCell.RowNumber];
            }
            else if (datagrid == this.outDataGrid1)
            {

                BindingList<TreatOut> mylist = (BindingList<TreatOut>)datagrid.DataSource;
                currentObject = (TreatOut)mylist[datagrid.CurrentCell.RowNumber];
            }

         //   MessageBox.Show(currentObject.thisUnitLoad.Name.ToString());
            Handletreat newForm = new Handletreat(currentObject);
            this.Hide();
            newForm.Show();
        }
    }
}