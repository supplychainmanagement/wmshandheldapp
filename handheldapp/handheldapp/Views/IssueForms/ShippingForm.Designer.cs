﻿namespace handheldapp.Views.IssueForms
{
    partial class ShippingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.iodNoLabel = new System.Windows.Forms.Label();
            this.itemSriallabel = new System.Windows.Forms.Label();
            this.qtyLabel = new System.Windows.Forms.Label();
            this.goButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dNameTextBox = new System.Windows.Forms.TextBox();
            this.shippingAgencyTextBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.backButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.shipJobId = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 20);
            this.label1.Text = "Shipping Issue order No";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.Text = "Item No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(134, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 20);
            this.label3.Text = "Qty";
            // 
            // iodNoLabel
            // 
            this.iodNoLabel.Location = new System.Drawing.Point(155, 25);
            this.iodNoLabel.Name = "iodNoLabel";
            this.iodNoLabel.Size = new System.Drawing.Size(100, 20);
            this.iodNoLabel.Text = "0000";
            // 
            // itemSriallabel
            // 
            this.itemSriallabel.Location = new System.Drawing.Point(61, 45);
            this.itemSriallabel.Name = "itemSriallabel";
            this.itemSriallabel.Size = new System.Drawing.Size(67, 20);
            this.itemSriallabel.Text = "0000";
            // 
            // qtyLabel
            // 
            this.qtyLabel.Location = new System.Drawing.Point(166, 45);
            this.qtyLabel.Name = "qtyLabel";
            this.qtyLabel.Size = new System.Drawing.Size(67, 20);
            this.qtyLabel.Text = "0000";
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(14, 179);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(230, 20);
            this.goButton.TabIndex = 10;
            this.goButton.Text = "Go";
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.Text = "Driver Name";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.Text = "Shipping Agency";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 20);
            this.label6.Text = "Date\r\n";
            // 
            // dNameTextBox
            // 
            this.dNameTextBox.Location = new System.Drawing.Point(103, 74);
            this.dNameTextBox.Name = "dNameTextBox";
            this.dNameTextBox.Size = new System.Drawing.Size(141, 23);
            this.dNameTextBox.TabIndex = 17;
            this.dNameTextBox.TextChanged += new System.EventHandler(this.dNameTextBox_TextChanged);
            // 
            // shippingAgencyTextBox
            // 
            this.shippingAgencyTextBox.Location = new System.Drawing.Point(103, 109);
            this.shippingAgencyTextBox.Name = "shippingAgencyTextBox";
            this.shippingAgencyTextBox.Size = new System.Drawing.Size(141, 23);
            this.shippingAgencyTextBox.TabIndex = 18;
            this.shippingAgencyTextBox.TextChanged += new System.EventHandler(this.shippingAgencyTextBox_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(103, 141);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(141, 24);
            this.dateTimePicker1.TabIndex = 19;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(21, 218);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(212, 20);
            this.backButton.TabIndex = 20;
            this.backButton.Text = "Back";
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 20);
            this.label7.Text = "Shippin Job";
            // 
            // shipJobId
            // 
            this.shipJobId.Location = new System.Drawing.Point(103, 5);
            this.shipJobId.Name = "shipJobId";
            this.shipJobId.Size = new System.Drawing.Size(100, 20);
            this.shipJobId.Text = "0000";
            // 
            // ShippingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(258, 268);
            this.Controls.Add(this.shipJobId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.shippingAgencyTextBox);
            this.Controls.Add(this.dNameTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.qtyLabel);
            this.Controls.Add(this.itemSriallabel);
            this.Controls.Add(this.iodNoLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "ShippingForm";
            this.Text = " ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label iodNoLabel;
        private System.Windows.Forms.Label itemSriallabel;
        private System.Windows.Forms.Label qtyLabel;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox dNameTextBox;
        private System.Windows.Forms.TextBox shippingAgencyTextBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label shipJobId;
    }
}