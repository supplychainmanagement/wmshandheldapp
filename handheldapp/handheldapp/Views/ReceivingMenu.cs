﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Controllers;
namespace handheldapp.Views
{
    public partial class ReceivingMenu : Form
    {
        public ReceivingMenu()
        {
            InitializeComponent();
        }

        private void ReceivingMenu_Load(object sender, EventArgs e)
        {
            GlobalData.previous_form = new homescreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ItemsOfPOD_view newForm = new ItemsOfPOD_view();
            this.Hide();
            newForm.Show();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string STJ_Serial = ReceiveController.generateAstorageJobSerial(); 
            Submit_STJ newForm = new Submit_STJ(STJ_Serial);
            this.Hide();
            newForm.Show();
        }
    }
}