﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Model;
using handheldapp.Controllers;
using handheldapp.Connection;

namespace handheldapp.Views
{
    public partial class Submit_STJ : Form
    {
        int step=0; //0 is now scanning the unitload , 1 is scanning the storage location
       
        string stlSerial;
        string UL_Serial; 
        String STJSerial;
        
        public Submit_STJ()
        {
            InitializeComponent();
            GlobalData.previous_form = new ReceivingMenu();
        }
        private bool basicValidation() {

            if (step == 1)
            {
                //storage serial Basic Validation.
                if (stlSerial == null || stlSerial == "")
                {
                    MessageBox.Show("Please scan the storage Location", "[[Error]]");
                    return false;
                }
                if (!GlobalData.IsDigitsOnly(stlSerial))
                {
                    MessageBox.Show("Storage Location is not in correct format", "[[Error]]");
                    return false;
                }
            }

            if (step == 0)
            {
                // unitload validation  
                if (UL_Serial == null || UL_Serial == "")
                {
                    MessageBox.Show("Please scan the unitload", "[[Error]]");
                    return false;
                }

                if (!GlobalData.IsDigitsOnly(UL_Serial))
                {
                    MessageBox.Show("Unitload Serial is not in correct format", "[[Error]]");
                    return false;
                }
            }
            return true; 
        }

        private bool logicValidation()
        {
            return true;
        }
        public Submit_STJ(String RCJSerial_)
        {
            InitializeComponent();
            GlobalData.previous_form = new ReceivingMenu();
            STJSerial = RCJSerial_;
            this.stjnoLabel.Text = "" + STJSerial;  

        }

        
        private void backButton_Click(object sender, EventArgs e)
        {

        }

        private void backButton_Click_1(object sender, EventArgs e)
        {

            this.Hide();
            GlobalData.previous_form.Show();
        }

      

        private void ulSerialGoButton_Click(object sender, EventArgs e)
        {
            if (step == 0)// scanning the item 
            {
                if (!basicValidation()) return;
                if (!logicValidation()) return;
                //Devisible the the item controls 
                ulSerialLabel.Visible = false;
                ul_Serial_textBox.Visible = false; 

                //Visible the Ul Controls
                stlSerialLabel.Visible = true;
                stlSerialTextBox.Visible = true;

                step++; 

               
            }
            else if (step == 1) {
                
                if (!basicValidation()) return;
                if (!logicValidation()) return;


                Reply returnValue = ReceiveController.SubmitStorageJob(STJSerial, stlSerial, UL_Serial);
                String ResultViewer = "Reply is "+ returnValue.sCode + " "+ returnValue.payload+"\n"+
                                       " " + STJSerial + " " + UL_Serial + " " + stlSerial+"";
                MessageBox.Show(ResultViewer) ;
            }

            }

        private void stlSerialTextBox_TextChanged(object sender, EventArgs e)
        {
            stlSerial = stlSerialTextBox.Text;
        }


        private void ul_Serial_textBox_TextChanged(object sender, EventArgs e)
        {
            UL_Serial = ul_Serial_textBox.Text;
        }

  
       

        private void itemSerialLabel_ParentChanged(object sender, EventArgs e)
        {

        }
        }
}
