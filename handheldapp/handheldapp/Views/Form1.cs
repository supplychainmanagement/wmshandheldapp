﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Views;
using handheldapp.Views.IssueForms;
namespace handheldapp
{
    public partial class homescreen : Form
    {
        public homescreen()
        {
            InitializeComponent();
        }

        private void notImplementedYetMsgBox(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented yet", "[INFO]");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            test my = new test();
            my.Show();
        }

        private void test(object sender, EventArgs e)
        {
            ReceivingMenu newForm = new ReceivingMenu();
            this.Hide();
            newForm.Show();
        }

        private void issue(object sender, EventArgs e)
        {
            IssueMenu newForm = new IssueMenu();
            this.Hide();
            newForm.Show();
        }
    }
}