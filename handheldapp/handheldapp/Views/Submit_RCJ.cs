﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using handheldapp.Model;
using handheldapp.Controllers;
using handheldapp.Connection;

namespace handheldapp.Views
{
    public partial class Submit_RCJ : Form
    {
        int step=0; //0 is now scanning the item , 1 is scanning the uniload
        int amount;
        string itemSerial;
        string UL_Serial; 
        int ItemQulatiyStatus;
        DateTime productionDate;
        PurchaseOrderItem purchaseOrderItem;
        String RCJSerial;
        public Submit_RCJ()
        {
            InitializeComponent();
            GlobalData.previous_form = new ItemsOfPOD_view();
        }
        private bool basicValidation() {

            if (step == 0)
            {
                //Item serial Basic Validation.
                if (itemSerial == null || itemSerial == "")
                {
                    MessageBox.Show("Please scan the item", "[[Error]]");
                    return false;
                }
                if (!GlobalData.IsDigitsOnly(itemSerial))
                {
                    MessageBox.Show("Item Serial is not in correct format", "[[Error]]");
                    return false;
                }
            }

            if (step == 1)
            {
                // unitload validation  
                if (UL_Serial == null || UL_Serial == "")
                {
                    MessageBox.Show("Please scan the unitload", "[[Error]]");
                    return false;
                }

                if (!GlobalData.IsDigitsOnly(UL_Serial))
                {
                    MessageBox.Show("Unitload Serial is not in correct format", "[[Error]]");
                    return false;
                }
            }
            return true; 
        }

        private bool logicValidation()
        {

            //Serial Matching Validation 

            if (!itemSerial.Equals(purchaseOrderItem.ItemSerial)) {
                MessageBox.Show("Item Serials Missmatch", "[[Error]]");
                return false;
            }
            /// To be added to the init numeric value 3la tool 
            if (amount > purchaseOrderItem.Amount) {
                MessageBox.Show("Amount must be less than the total order item amount", "[[Error]]");
                return false;
            }
            return true;
        }
        public Submit_RCJ(PurchaseOrderItem purchaseOrderItem_,String RCJSerial_)
        {
            InitializeComponent();
            GlobalData.previous_form = new ItemsOfPOD_view();
            purchaseOrderItem = purchaseOrderItem_;
            RCJSerial = RCJSerial_;
            this.rcjnoLabel.Text = "" + RCJSerial;  

        }

        private void productionDatecheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            productionDatePicker1.Enabled = productionDatecheckBox.Checked;

            if (!productionDatecheckBox.Checked)
            {
                productionDate = new DateTime(1900, 1, 1);
            }
            else
            {
                productionDate = productionDatePicker1.Value;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {

        }

        private void backButton_Click_1(object sender, EventArgs e)
        {

            this.Hide();
            GlobalData.previous_form.Show();
        }

        private void Submit_RCJ_Load(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = new List<string> { "Released", "Quarantined", "Blocked" };
            QScomboBox1.DataSource = bs;
        }

        private void ulSerialGoButton_Click(object sender, EventArgs e)
        {
            if (step == 0)// scanning the item 
            {
                if (!basicValidation()) return;
                if (!logicValidation()) return;
                //Devisible the the item controls 
                itemSerialTextBox.Visible = false;
                itemSerialLabel.Visible = false; 

                //Visible the Ul Controls
                ul_Serial_textBox.Visible = true;
                ulSerialLabel.Visible = true;

                step++; 

               
            }
            else if (step == 1) {
                
                if (!basicValidation()) return;
                if (!logicValidation()) return;
                

               Reply returnValue= ReceiveController.SubmitReceivigJob(RCJSerial,purchaseOrderItem.id,amount,itemSerial,
                   UL_Serial,ItemQulatiyStatus,productionDate) ;
                String ResultViewer = "Reply is "+ returnValue.sCode + " "+ returnValue.payload+"\n"+
                                       " " +RCJSerial+" " +purchaseOrderItem.id+" " +amount+" " +itemSerial+" " + UL_Serial+" " +ItemQulatiyStatus+" " +productionDate ;
                MessageBox.Show(ResultViewer) ;
            }

            }

        private void itemSerialTextBox_TextChanged(object sender, EventArgs e)
        {
            itemSerial = itemSerialTextBox.Text;
        }

        private void ul_Serial_textBox_TextChanged(object sender, EventArgs e)
        {
            UL_Serial = ul_Serial_textBox.Text;
        }

        private void amountlabel_ValueChanged(object sender, EventArgs e)
        {
            amount = (int)amountlabel.Value;
        }

        private void QScomboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemQulatiyStatus = QScomboBox1.SelectedIndex;
        }

        private void productionDatePicker1_ValueChanged(object sender, EventArgs e)
        {
            productionDate = productionDatePicker1.Value;
        }
        }
}
