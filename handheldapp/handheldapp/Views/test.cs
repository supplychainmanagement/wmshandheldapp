﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using handheldapp.Connection;
using System.IO; 
namespace handheldapp.Views
{
    public partial class test : Form
    {
        public test()
        {
            InitializeComponent();
        }

        private void test_Load(object sender, EventArgs e)
        {
            string responseText = "{\"id\": 1,\"pod_num\": \"ABCDED\",\"date\": \"2016-08-11\","+
               " \"clientId\": 1,  \"clientName\": \"a a\"}";

            JsonTextReader reader = new JsonTextReader(new StringReader(responseText));
            while (reader.Read())
            {
                if (reader.Value != null)
               {
                   textBox1.Text+=("Token: {0}, Value: {1}"+ reader.TokenType+ " "+ reader.Value)+"\n";
                }
                else
                {
                    textBox1.Text+=("Token: {0}"+ reader.TokenType)+"\n";
                }
            }
            
        }
    }
}