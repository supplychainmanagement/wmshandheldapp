﻿namespace handheldapp.Views
{
    partial class Submit_RCJ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.rcjnoLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemSerialTextBox = new System.Windows.Forms.TextBox();
            this.ulSerialGoButton = new System.Windows.Forms.Button();
            this.ulSerialLabel = new System.Windows.Forms.Label();
            this.ul_Serial_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.amountlabel = new System.Windows.Forms.NumericUpDown();
            this.itemSerialLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.productionDatePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.QScomboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.productionDatecheckBox = new System.Windows.Forms.CheckBox();
            this.backButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 47);
            this.label1.Text = "Proceeding a receiving job with number: ";
            // 
            // rcjnoLabel
            // 
            this.rcjnoLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.rcjnoLabel.Location = new System.Drawing.Point(73, 34);
            this.rcjnoLabel.Name = "rcjnoLabel";
            this.rcjnoLabel.Size = new System.Drawing.Size(146, 20);
            this.rcjnoLabel.Text = "00000";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.itemSerialTextBox);
            this.panel1.Controls.Add(this.ulSerialGoButton);
            this.panel1.Controls.Add(this.ulSerialLabel);
            this.panel1.Controls.Add(this.ul_Serial_textBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.amountlabel);
            this.panel1.Controls.Add(this.itemSerialLabel);
            this.panel1.Location = new System.Drawing.Point(6, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 128);
            // 
            // itemSerialTextBox
            // 
            this.itemSerialTextBox.Location = new System.Drawing.Point(6, 65);
            this.itemSerialTextBox.Name = "itemSerialTextBox";
            this.itemSerialTextBox.Size = new System.Drawing.Size(228, 23);
            this.itemSerialTextBox.TabIndex = 5;
            this.itemSerialTextBox.TextChanged += new System.EventHandler(this.itemSerialTextBox_TextChanged);
            // 
            // ulSerialGoButton
            // 
            this.ulSerialGoButton.Location = new System.Drawing.Point(13, 94);
            this.ulSerialGoButton.Name = "ulSerialGoButton";
            this.ulSerialGoButton.Size = new System.Drawing.Size(218, 20);
            this.ulSerialGoButton.TabIndex = 4;
            this.ulSerialGoButton.Text = "Go";
            this.ulSerialGoButton.Click += new System.EventHandler(this.ulSerialGoButton_Click);
            // 
            // ulSerialLabel
            // 
            this.ulSerialLabel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.ulSerialLabel.Location = new System.Drawing.Point(6, 42);
            this.ulSerialLabel.Name = "ulSerialLabel";
            this.ulSerialLabel.Size = new System.Drawing.Size(210, 20);
            this.ulSerialLabel.Text = "Scan The Unit Load Serial ";
            this.ulSerialLabel.Visible = false;
            // 
            // ul_Serial_textBox
            // 
            this.ul_Serial_textBox.Location = new System.Drawing.Point(6, 65);
            this.ul_Serial_textBox.Name = "ul_Serial_textBox";
            this.ul_Serial_textBox.Size = new System.Drawing.Size(234, 23);
            this.ul_Serial_textBox.TabIndex = 5;
            this.ul_Serial_textBox.Visible = false;
            this.ul_Serial_textBox.TextChanged += new System.EventHandler(this.ul_Serial_textBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 20);
            this.label3.Text = "Specify The Amount";
            // 
            // amountlabel
            // 
            this.amountlabel.Location = new System.Drawing.Point(178, 7);
            this.amountlabel.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.amountlabel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountlabel.Name = "amountlabel";
            this.amountlabel.Size = new System.Drawing.Size(53, 24);
            this.amountlabel.TabIndex = 1;
            this.amountlabel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amountlabel.ValueChanged += new System.EventHandler(this.amountlabel_ValueChanged);
            // 
            // itemSerialLabel
            // 
            this.itemSerialLabel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.itemSerialLabel.Location = new System.Drawing.Point(3, 42);
            this.itemSerialLabel.Name = "itemSerialLabel";
            this.itemSerialLabel.Size = new System.Drawing.Size(185, 20);
            this.itemSerialLabel.Text = "Scan The Item serial ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.productionDatePicker1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.QScomboBox1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.productionDatecheckBox);
            this.panel2.Location = new System.Drawing.Point(3, 206);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 87);
            // 
            // productionDatePicker1
            // 
            this.productionDatePicker1.Enabled = false;
            this.productionDatePicker1.Location = new System.Drawing.Point(147, 49);
            this.productionDatePicker1.Name = "productionDatePicker1";
            this.productionDatePicker1.Size = new System.Drawing.Size(84, 24);
            this.productionDatePicker1.TabIndex = 11;
            this.productionDatePicker1.ValueChanged += new System.EventHandler(this.productionDatePicker1_ValueChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 20);
            this.label6.Text = "Production Date";
            // 
            // QScomboBox1
            // 
            this.QScomboBox1.Location = new System.Drawing.Point(134, 8);
            this.QScomboBox1.Name = "QScomboBox1";
            this.QScomboBox1.Size = new System.Drawing.Size(100, 23);
            this.QScomboBox1.TabIndex = 8;
            this.QScomboBox1.SelectedIndexChanged += new System.EventHandler(this.QScomboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 20);
            this.label4.Text = "Quality Status";
            // 
            // productionDatecheckBox
            // 
            this.productionDatecheckBox.Location = new System.Drawing.Point(116, 53);
            this.productionDatecheckBox.Name = "productionDatecheckBox";
            this.productionDatecheckBox.Size = new System.Drawing.Size(100, 20);
            this.productionDatecheckBox.TabIndex = 12;
            this.productionDatecheckBox.CheckStateChanged += new System.EventHandler(this.productionDatecheckBox_CheckStateChanged);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(19, 311);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(200, 20);
            this.backButton.TabIndex = 8;
            this.backButton.Text = "Back";
            this.backButton.Click += new System.EventHandler(this.backButton_Click_1);
            // 
            // Submit_RCJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(258, 347);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rcjnoLabel);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "Submit_RCJ";
            this.Text = "Submit Receiving Job";
            this.Load += new System.EventHandler(this.Submit_RCJ_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label rcjnoLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label itemSerialLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown amountlabel;
        private System.Windows.Forms.TextBox itemSerialTextBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox ul_Serial_textBox;
        private System.Windows.Forms.Button ulSerialGoButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label ulSerialLabel;
        private System.Windows.Forms.ComboBox QScomboBox1;
        private System.Windows.Forms.DateTimePicker productionDatePicker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox productionDatecheckBox;
        private System.Windows.Forms.Button backButton;
    }
}