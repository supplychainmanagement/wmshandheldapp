﻿namespace handheldapp.Views
{
    partial class Submit_STJ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.stjnoLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.stlSerialTextBox = new System.Windows.Forms.TextBox();
            this.ulSerialGoButton = new System.Windows.Forms.Button();
            this.ulSerialLabel = new System.Windows.Forms.Label();
            this.ul_Serial_textBox = new System.Windows.Forms.TextBox();
            this.stlSerialLabel = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // stjnoLabel
            // 
            this.stjnoLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.stjnoLabel.Location = new System.Drawing.Point(73, 34);
            this.stjnoLabel.Name = "stjnoLabel";
            this.stjnoLabel.Size = new System.Drawing.Size(200, 20);
            this.stjnoLabel.Text = "00000";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.stlSerialTextBox);
            this.panel1.Controls.Add(this.ulSerialGoButton);
            this.panel1.Controls.Add(this.ulSerialLabel);
            this.panel1.Controls.Add(this.ul_Serial_textBox);
            this.panel1.Controls.Add(this.stlSerialLabel);
            this.panel1.Location = new System.Drawing.Point(3, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 108);
            // 
            // stlSerialTextBox
            // 
            this.stlSerialTextBox.Location = new System.Drawing.Point(3, 36);
            this.stlSerialTextBox.Name = "stlSerialTextBox";
            this.stlSerialTextBox.Size = new System.Drawing.Size(218, 23);
            this.stlSerialTextBox.TabIndex = 5;
            this.stlSerialTextBox.Visible = false;
            this.stlSerialTextBox.WordWrap = false;
            this.stlSerialTextBox.TextChanged += new System.EventHandler(this.stlSerialTextBox_TextChanged);
            // 
            // ulSerialGoButton
            // 
            this.ulSerialGoButton.Location = new System.Drawing.Point(16, 74);
            this.ulSerialGoButton.Name = "ulSerialGoButton";
            this.ulSerialGoButton.Size = new System.Drawing.Size(205, 20);
            this.ulSerialGoButton.TabIndex = 4;
            this.ulSerialGoButton.Text = "Go";
            this.ulSerialGoButton.Click += new System.EventHandler(this.ulSerialGoButton_Click);
            // 
            // ulSerialLabel
            // 
            this.ulSerialLabel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.ulSerialLabel.Location = new System.Drawing.Point(3, 4);
            this.ulSerialLabel.Name = "ulSerialLabel";
            this.ulSerialLabel.Size = new System.Drawing.Size(218, 20);
            this.ulSerialLabel.Text = "Scan The Unit Load Serial";
            // 
            // ul_Serial_textBox
            // 
            this.ul_Serial_textBox.Location = new System.Drawing.Point(3, 36);
            this.ul_Serial_textBox.Name = "ul_Serial_textBox";
            this.ul_Serial_textBox.Size = new System.Drawing.Size(218, 23);
            this.ul_Serial_textBox.TabIndex = 5;
            this.ul_Serial_textBox.TextChanged += new System.EventHandler(this.ul_Serial_textBox_TextChanged);
            // 
            // stlSerialLabel
            // 
            this.stlSerialLabel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.stlSerialLabel.Location = new System.Drawing.Point(3, 4);
            this.stlSerialLabel.Name = "stlSerialLabel";
            this.stlSerialLabel.Size = new System.Drawing.Size(218, 20);
            this.stlSerialLabel.Text = "Scan The storage location serial ";
            this.stlSerialLabel.Visible = false;
            this.stlSerialLabel.ParentChanged += new System.EventHandler(this.itemSerialLabel_ParentChanged);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(19, 181);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(205, 20);
            this.backButton.TabIndex = 8;
            this.backButton.Text = "Back";
            this.backButton.Click += new System.EventHandler(this.backButton_Click_1);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 47);
            this.label1.Text = "Proceeding a Storage  job with number: ";
            // 
            // Submit_STJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(258, 224);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.stjnoLabel);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "Submit_STJ";
            this.Text = "Submit Storage Job";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label stjnoLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label stlSerialLabel;
        private System.Windows.Forms.TextBox stlSerialTextBox;
        private System.Windows.Forms.TextBox ul_Serial_textBox;
        private System.Windows.Forms.Button ulSerialGoButton;
        private System.Windows.Forms.Label ulSerialLabel;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label label1;
    }
}