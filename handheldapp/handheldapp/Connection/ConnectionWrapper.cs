﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
namespace handheldapp.Connection
{
    public static class ConnectionWrapper
    {

        private static String autenticationToken = "";

        public static String AutenticationToken
        {
            get { return ConnectionWrapper.autenticationToken; }
            set { ConnectionWrapper.autenticationToken = value; }
        }
        public static HTTPResponse sendRequest(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers["Authorization"] = getTocken();
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return new HTTPResponse((int)httpResponse.StatusCode, streamReader.ReadToEnd());
            }
        }

        public static HTTPResponse PostRequest(string url, Dictionary<string, string> fields)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers["Authorization"] = getTocken();
            httpWebRequest.AllowWriteStreamBuffering = true;
            using (StreamWriter sw = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                foreach (KeyValuePair<string, string> entry in fields)
                {
                    sw.Write(entry.Key + "=" + entry.Value + "&");
                }
                sw.Flush();
                sw.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return new HTTPResponse((int)httpResponse.StatusCode, streamReader.ReadToEnd());
                }
            }
            catch (System.Net.WebException webException)
            {
                MessageBox.Show(webException.Message);
                return null;
            }
        }

        private static string getTocken()
        {

            return autenticationToken;
            /*
            string path = "tocken.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("the file could not be read/n" + error.Message);
                return "";
            }*/
        }
    }
}
