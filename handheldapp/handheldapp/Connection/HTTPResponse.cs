﻿using System;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Connection
{
    public class HTTPResponse
    {
        public int statusCode { get; set; }
        public string response { get; set; }

        public HTTPResponse(int statusCode, string response)
        {
            this.statusCode = statusCode;
            this.response = response;
        }
    }
}
