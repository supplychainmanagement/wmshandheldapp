﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace UHFWinCE.HTTP
{
    static class HTTPRequest
    {
        public static UHFWinCE.HTTP.HTTPResponse sendRequest(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers["Authorization"] = getTocken();
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return new UHFWinCE.HTTP.HTTPResponse((int)httpResponse.StatusCode,streamReader.ReadToEnd());
            }
        }

        public static UHFWinCE.HTTP.HTTPResponse PostRequest(string url, Dictionary<string, string> fields)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers["Authorization"] = getTocken();
            httpWebRequest.AllowWriteStreamBuffering = true;
            using (StreamWriter sw = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                foreach (KeyValuePair<string, string> entry in fields)
                {
                    sw.Write(entry.Key + "=" + entry.Value + "&");
                }
                sw.Flush();
                sw.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return new UHFWinCE.HTTP.HTTPResponse((int)httpResponse.StatusCode, streamReader.ReadToEnd());
                }
            }
            catch (System.Net.WebException webException)
            {
                MessageBox.Show(webException.Message);
                return null;
            }         
        }

        private static string getTocken()
        {
            string path = "tocken.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("the file could not be read/n" + error.Message);
                return "";
            }
        }
    }
}
