﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class Client
    {

        public Client() { }

        public Client(string id_, string name_)
        {
            id = id_;
            name = name_;
        }

        string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        } 

    }
}
