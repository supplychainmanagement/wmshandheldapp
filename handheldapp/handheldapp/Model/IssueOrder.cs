﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class IssueOrder
    {

        public IssueOrder() { 
        
        }
        public IssueOrder(String orderno_, Client client_, DateTime iodtime_ , Customer customer)
        {
            orderNo=orderno_;
            Client=client_;
            iodDate = iodtime_;
            Customer = customer;
        }

        string orderNo;

        public string OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        Client client;

        public Client Client
        {
            get { return client; }
            set { client = value; }
        }

        Customer customer;

        public Customer Customer
        {
            get { return customer; }
            set { customer = value; }
        }
        DateTime iodDate;

        public DateTime IodDate
        {
            get { return iodDate; }
            set { iodDate = value; }
        }

        List<IssueOrderItem> items;

        public List<IssueOrderItem> Items
        {
            get { return items; }
            set { items = value; }
        } 
    }
}
