﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class Rack
    {

        public Rack() { }

        public Rack(string id_, string name_, string serial_)
        {
            id = id_;
            name = name_;
            serial = serial_;
            this.thisZone = new Zone(id, "ZoneId " + id, "ZoneSerial " + serial);
        }

        string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        string serial;

        public string Serial
        {
            get { return serial; }
            set { serial = value; }
        }
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Zone thisZone;

    }
}
