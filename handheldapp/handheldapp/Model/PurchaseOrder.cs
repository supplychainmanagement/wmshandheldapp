﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class PurchaseOrder
    {

        public PurchaseOrder() { 
        
        }
        public PurchaseOrder(String orderno_, Client client_,DateTime podtime_ )
        {
            orderNo=orderno_;
            Client=client_;
            PodDate = podtime_;
        }

        string orderNo;

        public string OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        Client client;

        public Client Client
        {
            get { return client; }
            set { client = value; }
        }
        DateTime podDate;

        public DateTime PodDate
        {
            get { return podDate; }
            set { podDate = value; }
        }

        List<PurchaseOrderItem> items;

        public List<PurchaseOrderItem> Items
        {
            get { return items; }
            set { items = value; }
        } 
    }
}
