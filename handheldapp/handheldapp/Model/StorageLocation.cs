﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class StorageLocation
    {

        public StorageLocation() { }

        public StorageLocation(string id_, string name_, string serial_)
        {
            id = id_;
            name = name_;
            serial = serial_;
            this.thisRack = new Rack (id,"RackId "+id, "RackSerial "+ serial );
        }

        string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        string serial;

        public string Serial
        {
            get { return serial; }
            set { serial = value; }
        }
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Rack thisRack; 
    }
}
