﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class IssueOrderItem
    {
        public IssueOrderItem() { }
        public IssueOrderItem(string id_, ItemData itemData_, int amount_, string order_)
        {
            id = id_;
            itemData = itemData_;
            itemName = itemData.Name;
            itemSerial = itemData.Serial;
            amount = amount_;
            order = order_;
        }

        string order;

        public string Order
        {
            get { return order; }
            set { order = value; }
        }


        string itemName;

        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        } 


        string itemSerial;

        public string ItemSerial
        {
            get { return itemSerial; }
            set { itemSerial = value; }
        }

        int amount;

        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        int processed;

        public int Processed
        {
            get { return processed; }
            set { processed = value; }
        }

        public string id;


        public ItemData itemData;



        public IssueOrder iod;

       
    }
}
