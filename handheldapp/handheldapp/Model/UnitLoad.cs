﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class UnitLoad
    {

        public UnitLoad() { }

        public UnitLoad(string id_, string name_, string serial_)
        {
            id = id_;
            name = name_;
            serial = serial_;
            this.thisStorageLocation = new StorageLocation(id, "STLId " + id, "STLSerial " + serial);
        }

        string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        string serial;

        public string Serial
        {
            get { return serial; }
            set { serial = value; }
        }
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        StorageLocation thisStorageLocation;

        public StorageLocation ThisStorageLocation
        {
            get { return thisStorageLocation; }
            set { thisStorageLocation = value; }
        } 

    }
}
