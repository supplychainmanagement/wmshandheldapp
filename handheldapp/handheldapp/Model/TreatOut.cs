﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace handheldapp.Model
{
    public class TreatOut : Treat 
    {

        public TreatOut() {
            this.type = 1; 
        
        }

        
       

        public TreatOut(string id_, UnitLoad thisUnitLoad_, ItemData thisItemdata_)
        {
            id = id_;
            thisUnitLoad = thisUnitLoad_;
            this.thisItemdata = thisItemdata_;
            this.type = 1; 
        }

        
        int amount;

        /*Properties */
        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public string Route
        {
            get { return "Zone " + this.thisUnitLoad.ThisStorageLocation.thisRack.thisZone.Name + " Rack " + 
                            this.thisUnitLoad.ThisStorageLocation.thisRack.Name +
                            " STL " + this.thisUnitLoad.ThisStorageLocation.Name;
            }
        }

        public string UnitLoadName
        {
            get { return this.thisUnitLoad.Name; }
        }

        public string ItemName
        {
            get { return this.thisItemdata.Name; }
          
        } 


    }
}
